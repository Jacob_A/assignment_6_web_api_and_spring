package com.example.assignment_6_web_api_and_spring.controller;


import com.example.assignment_6_web_api_and_spring.mappers.CharactersMapper;
import com.example.assignment_6_web_api_and_spring.mappers.MoviesMapper;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.CreateMoviesDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.MoviesDTO;
import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.movies.MoviesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

// Mapping for movies, check CharacterController for descriptive comments, largely the same
@RestController
@RequestMapping(path = "api/v1/movies")
public class MoviesController {
   private final MoviesService moviesService;
   private final MoviesMapper moviesMapper;
   private final CharactersMapper charactersMapper;

    public MoviesController(MoviesService moviesService, MoviesMapper moviesMapper, CharactersMapper charactersMapper) {
        this.moviesService = moviesService;
        this.moviesMapper = moviesMapper;
        this.charactersMapper = charactersMapper;
    }

    @Operation(summary ="Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movies not found with supplied api",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity getAll() {
        Collection<MoviesDTO> chars = moviesMapper.moviesToMoviesDto(
                moviesService.findAll()
        );
        return ResponseEntity.ok(chars);
    }

    @Operation(summary ="Get movies by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied id",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id){
        MoviesDTO movies = moviesMapper.moviesToMoviesDto(
                moviesService.findById(id)
        );
        return ResponseEntity.ok(movies);
    }
    @Operation(summary ="Create new movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Movie successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @PostMapping
    public ResponseEntity add(@RequestBody CreateMoviesDTO newMov){
        Movies newMovie = moviesMapper.moviesDtoToMovies(newMov);
        Movies movie = moviesService.add(newMovie);
        URI location = URI.create(("movies/" + movie.getId()));
        return ResponseEntity.created(location).build();
    }
    @Operation(summary ="Update movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MoviesDTO moviesDTO, @PathVariable int id){
        if (id != moviesDTO.getId())
            return ResponseEntity.badRequest().build();
            moviesService.update(
                moviesMapper.moviesDtoToMovies(moviesDTO)
        );
        return ResponseEntity.noContent().build();
    }
    @Operation(summary ="Delete movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Movies not found with supplied id",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        moviesService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary ="Get all characters in movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters in the movie was successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "There are no movies with that id",
                    content = @Content)

    })
    @GetMapping("/{id}/characters")
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<CharactersDTO> charactersById = charactersMapper.characterToCharacterDto(
                moviesService.findById(id).getCharacters()
        );
        return ResponseEntity.ok(charactersById);
    }

}
