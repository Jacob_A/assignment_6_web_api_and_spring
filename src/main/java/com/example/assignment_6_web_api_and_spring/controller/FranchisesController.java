package com.example.assignment_6_web_api_and_spring.controller;

import com.example.assignment_6_web_api_and_spring.mappers.CharactersMapper;
import com.example.assignment_6_web_api_and_spring.mappers.FranchisesMapper;
import com.example.assignment_6_web_api_and_spring.mappers.MoviesMapper;
import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.franchises.CreateFranchisesDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.franchises.FranchisesDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.MoviesDTO;
import com.example.assignment_6_web_api_and_spring.models.Franchises;
import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.franchises.FranchisesService;
import com.example.assignment_6_web_api_and_spring.service.movies.MoviesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

// Mapping for franchises, check CharacterController for descriptive comments, largely the same
@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchisesController {
    public final FranchisesService franchisesService;
    public final MoviesMapper moviesMapper;
    public final CharactersMapper charactersMapper;
    public final MoviesService moviesService;
    public final FranchisesMapper franchisesMapper;

    public FranchisesController(FranchisesService franchisesService, MoviesMapper moviesMapper, CharactersMapper charactersMapper, MoviesService moviesService, FranchisesMapper franchisesMapper) {
        this.franchisesService = franchisesService;
        this.moviesMapper = moviesMapper;
        this.charactersMapper = charactersMapper;
        this.moviesService = moviesService;
        this.franchisesMapper = franchisesMapper;
    }

    @Operation(summary ="Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchises successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchises not found with supplied api",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity getAll() {
        Collection<FranchisesDTO> franc = franchisesMapper.franchisesToFranchisesDto(
                franchisesService.findAll()
        );
        return ResponseEntity.ok(franc);
    }

    @Operation(summary ="Get franchises by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "franchise successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with supplied id",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id){
        FranchisesDTO franc = franchisesMapper.franchisesToFranchisesDto(
                franchisesService.findById(id)
        );
        return ResponseEntity.ok(franc);
    }

    @Operation(summary ="Create new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "franchise successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @PostMapping
    public ResponseEntity add(@RequestBody CreateFranchisesDTO newFranc){
        Franchises newFranchise = franchisesMapper.franchisesDtoToFranchises(newFranc);
        Franchises franchise = franchisesService.add(newFranchise);
        URI location = URI.create(("franchises/" + franchise.getId()));
        return ResponseEntity.created(location).build();
    }
    @Operation(summary ="Update franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody FranchisesDTO franchisesDto, @PathVariable int id){
        if (id != franchisesDto.getId())
            return ResponseEntity.badRequest().build();
        franchisesService.update(
                franchisesMapper.franchisesDtoToFranchises(franchisesDto)
        );
        return ResponseEntity.noContent().build();
    }


    @Operation(summary ="Delete franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Franchise not found with supplied id",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        franchisesService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    @Operation(summary ="Get all movies in franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies in the franchise found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "There are no franchise with that id",
                    content = @Content)

    })
    @GetMapping("/{id}/movies")
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<MoviesDTO> moviesById = moviesMapper.moviesToMoviesDto(
                franchisesService.findById(id).getMovies()
        );
        return ResponseEntity.ok(moviesById);
    }
    @Operation(summary ="Get all characters in all movies in the franchise by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters in the franchise found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "There are no franchise with that id",
                    content = @Content)

    })
    @GetMapping("/{id}/movies/characters")
    public ResponseEntity findCharacterWithIdInMovie(@PathVariable int id){
        var moviesFromFranchise = franchisesService.findById(id).getMovies();
        Set<Characters> collection = new HashSet<>();

          for (Movies movies : moviesFromFranchise) {
            collection.addAll(movies.getCharacters());
        }
        Collection<CharactersDTO> characterFromFranchise = charactersMapper.characterToCharacterDto(
                collection
        );
        return ResponseEntity.ok(characterFromFranchise);
    }
}
