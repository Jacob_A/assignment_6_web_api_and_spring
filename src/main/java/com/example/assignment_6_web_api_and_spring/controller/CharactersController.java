package com.example.assignment_6_web_api_and_spring.controller;

import com.example.assignment_6_web_api_and_spring.mappers.CharactersMapper;
import com.example.assignment_6_web_api_and_spring.mappers.MoviesMapper;
import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CreateCharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.MoviesDTO;
import com.example.assignment_6_web_api_and_spring.service.characters.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

// Mapping to the DB for the characters, set the default mapping to /characters
// The mapping is largely the same for the other two controllers, comments will refer back to this one
@RestController
@RequestMapping(path = "api/v1/characters")
public class CharactersController {

    private final CharacterService characterService;
    private final CharactersMapper charactersMapper;
    private final MoviesMapper moviesMapper;

    public CharactersController(CharacterService characterService, CharactersMapper charactersMapper, MoviesMapper moviesMapper) {
        this.characterService = characterService;
        this.charactersMapper = charactersMapper;
        this.moviesMapper = moviesMapper;
    }

// Documentation for OpenAPI/Swagger
@Operation(summary ="Get all characters")
@ApiResponses(value = {
        @ApiResponse(responseCode = "200",
        description = "Characters successfully found",
        content = @Content),
        @ApiResponse(responseCode = "404",
                description = "Characters not found with supplied api",
                content = @Content)
})
    // Returns all characters in BD
    @GetMapping
    public ResponseEntity getAll() {
        // Transforms our character object from the DB to a JSON appropriate format with the help of a DTO
        Collection<CharactersDTO> chars = charactersMapper.characterToCharacterDto(
                characterService.findAll()
        );
        return ResponseEntity.ok(chars);
    }

    @Operation(summary ="Get character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied id",
                    content = @Content)
    })
    // Returns character with specific ID
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id){
        CharactersDTO chars = charactersMapper.characterToCharacterDto(
                characterService.findById(id)
        );
        return ResponseEntity.ok(chars);
    }

    @Operation(summary ="Create a new character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Character successfully created",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    // Add a new character to the DB
    @PostMapping
    public ResponseEntity add(@RequestBody CreateCharactersDTO newCharacter){
        // Takes in the JSON format of a characters and transform it into DB data able to be handled by Postgres, with help of a mapping
        Characters newChar = charactersMapper.characterDTOToCharacter(newCharacter);
        Characters chars = characterService.add(newChar);
            URI location = URI.create(("characters/" + chars.getId()));
        return ResponseEntity.created(location).build();
    }

    @Operation(summary ="Update character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Properties cant be null or max length reached",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })

    // Updates a existing characters with a specified ID
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody CharactersDTO charsDTO, @PathVariable int id){
        if (id != charsDTO.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(
                charactersMapper.characterDtoToCharacter(charsDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary ="Delete character by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Characters successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "500",
                    description = "Characters not found with supplied id",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content)

    })
    // Delete a character in the DB with specified ID (Destructive operation, should normally not be done)
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary ="Get all movies the character by id is in")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies with the characters was successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "There are no characters with that id",
                    content = @Content)

    })

    // Fetch all movies specific to a character's ID
    @GetMapping("/{id}/movies")
    public ResponseEntity findMoviesWithId(@PathVariable int id){

        Collection<MoviesDTO> moviesById = moviesMapper.moviesToMoviesDto(
                characterService.findById(id).getMovies()
        );
        return ResponseEntity.ok(moviesById);
    }


}
