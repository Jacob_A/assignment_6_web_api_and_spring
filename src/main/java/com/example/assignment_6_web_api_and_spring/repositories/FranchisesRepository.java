package com.example.assignment_6_web_api_and_spring.repositories;

import com.example.assignment_6_web_api_and_spring.models.Franchises;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Standard repository for repository with no other functionality
@Repository
public interface FranchisesRepository extends JpaRepository<Franchises,Integer> {
}
