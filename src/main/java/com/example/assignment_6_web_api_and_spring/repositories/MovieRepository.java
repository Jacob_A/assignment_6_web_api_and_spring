package com.example.assignment_6_web_api_and_spring.repositories;

import com.example.assignment_6_web_api_and_spring.models.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Standard movies for character with no other functionality
@Repository
public interface MovieRepository extends JpaRepository <Movies,Integer>{
}
