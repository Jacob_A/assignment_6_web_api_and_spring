package com.example.assignment_6_web_api_and_spring;

import com.example.assignment_6_web_api_and_spring.repositories.CharacterRepository;
import com.example.assignment_6_web_api_and_spring.service.characters.CharacterServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment6WebApiAndSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(Assignment6WebApiAndSpringApplication.class, args);

    }

}
