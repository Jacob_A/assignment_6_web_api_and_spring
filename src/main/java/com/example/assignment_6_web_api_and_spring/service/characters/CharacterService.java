package com.example.assignment_6_web_api_and_spring.service.characters;

import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.service.CrudService;

public interface CharacterService extends CrudService<Characters,Integer> {
}
