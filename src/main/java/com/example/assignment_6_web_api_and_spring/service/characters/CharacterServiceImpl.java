package com.example.assignment_6_web_api_and_spring.service.characters;

import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.repositories.CharacterRepository;
import com.example.assignment_6_web_api_and_spring.service.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;


import java.util.Collection;
// CharacterService implementation with standard crud functionality
@Service
public class CharacterServiceImpl implements CharacterService{
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Characters findById(Integer id) {
        return characterRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Characters> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Characters add(Characters entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Characters update(Characters entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        characterRepository.deleteById(id);
    }
}
