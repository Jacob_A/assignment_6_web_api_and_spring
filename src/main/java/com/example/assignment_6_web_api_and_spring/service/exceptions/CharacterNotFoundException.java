package com.example.assignment_6_web_api_and_spring.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CharacterNotFoundException extends RuntimeException{

    // Exception if a fetch with an ID not in the DB is being used
    public CharacterNotFoundException(int id) {
        super("No character exists with id: " + id);
    }
}

