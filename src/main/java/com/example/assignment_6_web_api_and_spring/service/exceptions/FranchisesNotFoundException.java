package com.example.assignment_6_web_api_and_spring.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchisesNotFoundException extends RuntimeException{
    public FranchisesNotFoundException(int id) {
        super("No franchises exists with id: " + id);
    }

}
