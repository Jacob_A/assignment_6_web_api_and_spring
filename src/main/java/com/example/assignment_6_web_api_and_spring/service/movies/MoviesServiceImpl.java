package com.example.assignment_6_web_api_and_spring.service.movies;

import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.repositories.MovieRepository;
import com.example.assignment_6_web_api_and_spring.service.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
// MovieService implementation with standard crud functionality
@Service
public class MoviesServiceImpl implements MoviesService{
    private final MovieRepository movieRepository;

    public MoviesServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movies findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
    }
    @Override
    public Collection<Movies> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movies add(Movies entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movies update(Movies entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
    }
}
