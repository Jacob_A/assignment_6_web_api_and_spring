package com.example.assignment_6_web_api_and_spring.service.movies;

import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.CrudService;

public interface MoviesService extends CrudService<Movies,Integer> {
}
