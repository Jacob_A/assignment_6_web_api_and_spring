package com.example.assignment_6_web_api_and_spring.service.franchises;

import com.example.assignment_6_web_api_and_spring.models.Franchises;
import com.example.assignment_6_web_api_and_spring.service.CrudService;

public interface FranchisesService extends CrudService<Franchises,Integer> {
}
