package com.example.assignment_6_web_api_and_spring.service.franchises;

import com.example.assignment_6_web_api_and_spring.models.Franchises;
import com.example.assignment_6_web_api_and_spring.repositories.FranchisesRepository;
import com.example.assignment_6_web_api_and_spring.service.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.Collection;
// FranchisesService implementation with standard crud functionality
@Service
public class FranchisesServiceImpl implements FranchisesService{
private final FranchisesRepository franchisesRepository;

    public FranchisesServiceImpl(FranchisesRepository franchisesRepository) {
        this.franchisesRepository = franchisesRepository;
    }


    @Override
    public Franchises findById(Integer id) {
        return franchisesRepository.findById(id)
                .orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<Franchises> findAll() {
        return franchisesRepository.findAll();
    }

    @Override
    public Franchises add(Franchises entity) {
        return franchisesRepository.save(entity);
    }

    @Override
    public Franchises update(Franchises entity) {
        return franchisesRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
    franchisesRepository.deleteById(id);
    }
}
