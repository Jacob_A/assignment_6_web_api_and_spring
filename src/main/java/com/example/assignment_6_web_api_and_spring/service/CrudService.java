package com.example.assignment_6_web_api_and_spring.service;

import java.util.Collection;

// Interface for the service with CRUD functionality
public interface CrudService <T,ID>{
    T findById(ID id);

    Collection<T> findAll();

    T add(T entity);

    T update(T entity);

    void deleteById(ID id);
}
