package com.example.assignment_6_web_api_and_spring.models;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

// Model and relationship construct for movies entry in DB
@Entity
@Getter
@Setter
public class Movies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "movie_title",length = 50)
    private String title;

    @Column(name = "movie_genre" ,length = 50)
    private String genre;

    @Column(name = "movie_release_year",length = 100)
    private String releaseYear;

    @Column(name = "movie_director" , length = 50)
    private String director;

    @Column(name = "movie_picture")
    private String pictureUrl;

    @Column(name = "movie_trailer")
    private String trailerUrl;

    @ManyToOne
    @JoinColumn(name = "franc_id")
    private Franchises franchises;

    @ManyToMany(mappedBy = "movies")
    private Set<Characters> characters;
}

