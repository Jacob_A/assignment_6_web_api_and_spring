package com.example.assignment_6_web_api_and_spring.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

// Model and relationship construct for character entry in DB
@Entity
@Getter
@Setter
public class Characters {

    // Auto-incremental value for id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // Nullable makes the entry always requiring a name, length to prevent overflow on entry
    @Column(name = "char_name", length = 50, nullable = false)
    private String name;

    @Column(name = "char_alias", length = 50)
    private String alias;

    @Column(name = "char_gender" , length = 50)
    private String gender;

    @Column(name = "char_picture")
    private String pictureUrl;

    // Joins the table with movies, creates a linking table due to the many-to-many relationship
    @ManyToMany
    @JoinTable(
            name = "characters_movies",
            joinColumns = {@JoinColumn(name = "char_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )

    private Set<Movies> movies;
}

