package com.example.assignment_6_web_api_and_spring.models.Dtos.characters;

import lombok.Data;

import java.util.Set;

@Data
public class CharactersDTO {
// Standard character DTO with all fields for a character entry
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;
    private Set<Integer> movies;
}
