package com.example.assignment_6_web_api_and_spring.models.Dtos.franchises;

import lombok.Data;
@Data
public class CreateFranchisesDTO {
    // DTO to create a new franchise, removed movie list entry as it should be null when a franchise is created and linked afterwards
    private int id;
    private String name;
    private String description;
    private String director;
}
