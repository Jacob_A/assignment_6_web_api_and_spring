package com.example.assignment_6_web_api_and_spring.models.Dtos.movies;

import lombok.Data;

@Data
public class CreateMoviesDTO {
    // DTO to create a new movie, removed movie list entry and entry for a franchise, as it should be null when movies are created and linked afterwards
    private int id;
    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;
}
