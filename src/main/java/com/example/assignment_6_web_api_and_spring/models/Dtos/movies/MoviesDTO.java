package com.example.assignment_6_web_api_and_spring.models.Dtos.movies;

import lombok.Data;

import java.util.Set;

@Data
public class MoviesDTO {
    // Standard movie DTO with all fields for a movie entry
    private int id;
    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;
    private int franchises;
    private Set<Integer> characters;

}
