package com.example.assignment_6_web_api_and_spring.models.Dtos.franchises;

import lombok.Data;

import java.util.Set;
@Data
public class FranchisesDTO {
    // Standard franchise DTO with all fields for a franchise entry
    private int id;
    private String name;
    private String description;
    private String director;
    private Set<Integer> movies;
}
