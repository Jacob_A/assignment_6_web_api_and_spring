package com.example.assignment_6_web_api_and_spring.models.Dtos.characters;

import lombok.Data;

@Data
public class CreateCharactersDTO {
// DTO to create a new character, removed movie list entry as it should be null when character is created and linked afterwards
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;
}
