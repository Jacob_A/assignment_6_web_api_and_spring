package com.example.assignment_6_web_api_and_spring.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
// Model and relationship construct for franchises entry in DB
@Entity
@Getter
@Setter
public class Franchises {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "franc_name", length = 50)
    private String name;

    @Column(name = "franc_description",length = 150)
    private String description;

    @Column(name = "franc_director", length = 50)
    private String director;

    @OneToMany(mappedBy = "franchises")
    private Set<Movies> movies;

}
