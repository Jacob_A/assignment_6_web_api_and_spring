package com.example.assignment_6_web_api_and_spring.mappers;

import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.CreateMoviesDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.movies.MoviesDTO;
import com.example.assignment_6_web_api_and_spring.models.Franchises;
import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.characters.CharacterService;
import com.example.assignment_6_web_api_and_spring.service.franchises.FranchisesService;
import com.example.assignment_6_web_api_and_spring.service.movies.MoviesService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

// Mapper for movies, same structure as CharacterMapper
@Mapper(componentModel = "spring")
public abstract class MoviesMapper {
    @Autowired
    MoviesService moviesService;
    @Autowired
    CharacterService characterService;
    @Autowired
    FranchisesService franchisesService;

    @Mapping(target = "franchises", source = "franchises.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToId")
    public abstract MoviesDTO moviesToMoviesDto(Movies movies);

    public abstract Collection<MoviesDTO> moviesToMoviesDto(Collection<Movies> movies);

    @Mapping(target = "franchises", source = "franchises", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdToCharacter")
    public abstract Movies moviesDtoToMovies(MoviesDTO dto);

    @Mapping(target = "franchises", ignore = true)
    @Mapping(target = "characters", ignore = true)
    public abstract Movies moviesDtoToMovies(CreateMoviesDTO dto);

    @Named("franchiseIdToFranchise")
    Franchises mapIdToFranchise(int id){return franchisesService.findById(id);}

    @Named("characterIdToCharacter")
    Characters mapIdToCharacter(int id){

        return characterService.findById(id);}





    @Named("charactersToId")
    Set<Integer> mapCharactersToId (Set<Characters> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }



}
