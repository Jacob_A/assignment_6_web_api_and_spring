package com.example.assignment_6_web_api_and_spring.mappers;

import com.example.assignment_6_web_api_and_spring.models.Characters;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.characters.CreateCharactersDTO;
import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.characters.CharacterService;
import com.example.assignment_6_web_api_and_spring.service.movies.MoviesService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

// Mapper, create the abstraction to convert character data to our character DTOs and from DTO data to our character data
@Mapper(componentModel = "spring")
public abstract class CharactersMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected MoviesService moviesService;

    // Used for fetching data
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharactersDTO characterToCharacterDto(Characters character);
    public abstract Collection<CharactersDTO> characterToCharacterDto(Collection<Characters> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
    public abstract Characters characterDtoToCharacter(CharactersDTO dto);

    // Used to create a new character entry into the BD
    @Mapping(target="movies", ignore = true)
    public abstract Characters characterDTOToCharacter(CreateCharactersDTO dto);

    // Named functions for the mapping
    @Named("movieIdToMovies")
    Movies mapIdToMovies(int id) {
        return moviesService.findById(id);
    }


    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds (Set<Movies> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


}
