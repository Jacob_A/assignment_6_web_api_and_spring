package com.example.assignment_6_web_api_and_spring.mappers;

import com.example.assignment_6_web_api_and_spring.models.Dtos.franchises.CreateFranchisesDTO;
import com.example.assignment_6_web_api_and_spring.models.Dtos.franchises.FranchisesDTO;
import com.example.assignment_6_web_api_and_spring.models.Franchises;
import com.example.assignment_6_web_api_and_spring.models.Movies;
import com.example.assignment_6_web_api_and_spring.service.characters.CharacterService;
import com.example.assignment_6_web_api_and_spring.service.franchises.FranchisesService;
import com.example.assignment_6_web_api_and_spring.service.movies.MoviesService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

// Mapper for franchises, same structure as CharacterMapper
@Mapper(componentModel = "spring")
public abstract class FranchisesMapper {
    @Autowired
    MoviesService moviesService;
    @Autowired
    CharacterService characterService;
    @Autowired
    FranchisesService franchisesService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchisesDTO franchisesToFranchisesDto(Franchises franchises);

    public abstract Collection<FranchisesDTO> franchisesToFranchisesDto(Collection<Franchises> franchises);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
    public abstract Franchises franchisesDtoToFranchises(FranchisesDTO dto);

    @Mapping(target = "movies", ignore = true)
    public abstract Franchises franchisesDtoToFranchises(CreateFranchisesDTO dto);

    @Named("movieIdToMovies")
    Movies mapIdToMovies(int id) {
        return moviesService.findById(id);
    }

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds (Set<Movies> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
