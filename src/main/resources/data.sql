-- Franchises, Called first to be used in movies later
INSERT INTO franchises ("franc_name", franc_description, "franc_director") VALUES ('Marvel', 'Cinematic Universe', 'Stan Lee');
INSERT INTO franchises ("franc_name", franc_description, "franc_director") VALUES ('The Lord of the Ring', ' A masterpiece', 'Who knows..');

-- Movies
INSERT INTO movies ("movie_title", movie_genre, movie_release_year, movie_director, movie_picture, movie_trailer, franc_id) VALUES ('The Hulk', 'Action, Horror', '2004', 'Michel Bay', NULL, NULL, 1);
INSERT INTO movies ("movie_title", movie_genre, movie_release_year, movie_director, movie_picture, movie_trailer, franc_id) VALUES ('The Ironman', 'Action', '2010', 'Michel Bay', NULL, NULL, 1);
INSERT INTO movies ("movie_title", movie_genre, movie_release_year, movie_director, movie_picture, movie_trailer, franc_id) VALUES ('The Ascent of the Lords', 'Thriller', '2020', 'James Philip', NULL, NULL, 1);
INSERT INTO movies ("movie_title", movie_genre, movie_release_year, movie_director, movie_picture, movie_trailer, franc_id) VALUES ('Lord of the Rings', 'Action, Horror', '2002', 'Drake', NULL, NULL, 2);
-- Characters
INSERT into characters ("char_name", "char_alias", "char_gender", "char_picture") VALUES ('Tony Stark', 'Ironman', 'Male', NULL);
INSERT into characters ("char_name", "char_alias", "char_gender", "char_picture") VALUES ('Frodo baggins', 'RingBearer', 'Male', NULL);
INSERT into characters ("char_name", "char_alias", "char_gender", "char_picture") VALUES ('Samwise', 'FrodoBearer', 'Male', NULL);

-- Characters into movies (Joins many to many relation)
INSERT INTO characters_movies (char_id, movie_id) VALUES (1,2);
INSERT INTO characters_movies (char_id, movie_id) VALUES (2,4);
INSERT INTO characters_movies (char_id, movie_id) VALUES (3,4);
