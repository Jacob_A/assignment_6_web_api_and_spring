# Assignment 6 Access and expose a database

This is assignment 6 for Experis Academy, it is made in Java, using Springboot, Postgres as well as Docker. Documentation is done with the use of OpenAPI.

### Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work.

### Installing

To get the application running, simply clone the repo below:
```
git clone https://gitlab.com/Jacob_A/assignment_6_web_api_and_spring.git
```

### Usage

How to run the application:
```
* Open IntelliJ and use Run or Shift+F10 on the 'Assignment6WebApiAndSpringApplication'
```
#### Note: In order to run the program locally a DB connection needs to be added to the application.properties file

### Endpoints
* Heroku: https://assignment6-java.herokuapp.com
  
* Movie
    * **DELETE**: Delete a movie by id ```/api/v1/movies/{ID}```
    * **GET**: Get all movies ```/api/v1/movies/```
    * **GET**: Get movie by ID ```/api/v1/movies/{Id}```
    * **GET**: Get characters in a movie ```/api/v1/movies/{ID}/characters```
    * **POST**: Add new movie ```/api/v1/movies```
    * **PUT**: Update movie by ID```/api/v1/movies/{ID}```

* Franchise
    * **DELETE**: Delete a franchise by ID ```/api/v1/franchises/{ID}```
    * **GET**: Get all franchises ```/api/v1/franchises```
    * **GET**: Get franchise by ID ```/api/v1/franchise/{ID}```
    * **GET**: Get all movies in a franchise  ```/api/v1/franchise/{ID}/movies```
    * **GET**: Get characters in all movies in a franchise ```/api/v1/franchise/{ID}/movies/characters```
    * **POST**: Add new franchise ```/api/v1/franchise```
    * **PUT**: Update franchise ```/api/v1/franchise/{ID}```

* Character
    * **DELETE**: Delete a character by ID ```/api/v1/characters/{ID}```
    * **GET**: Get all characters ```/api/v1/characters/```
    * **GET**: Get character by ID ```/api/v1/characters/{ID}```
    * **GET**: Get all movies with character ```/api/v1/characters/{ID}/movies```
    * **POST**: Add new character ```/api/v1/characters```
    * **PUT**: Update character ```/api/v1/characters/{ID}```

### Contributors

Contributors and contact information

Jacob Andersson
[@Jacob_A](https://gitlab.com/Jacob_A/)

Robin Axelsson
[@Robinax](https://gitlab.com/Robinax)


### Contributing

PRs are not accepted